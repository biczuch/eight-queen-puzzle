﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.Converters
{
    class FieldColorToRectangleColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FieldColor fieldColor = (FieldColor) value;

            switch (fieldColor)
            {
                case FieldColor.Black:
                    return Brushes.Sienna;
                case FieldColor.White:
                    return Brushes.BurlyWood;
                default:
                    return Brushes.BurlyWood;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
