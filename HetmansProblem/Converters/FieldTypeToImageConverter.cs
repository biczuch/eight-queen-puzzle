﻿using System;
using System.Globalization;
using System.Windows.Data;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.Converters
{
    class FieldTypeToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FieldType fieldType = (FieldType) value;

            switch (fieldType)
            {
                case FieldType.Hetman:
                    return "Images/hetman.png";
                case FieldType.Checked:
                    return "Images/X.png";
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
