﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using HetmansProblem.Converters;
using HetmansProblem.Mechanics;
using HetmansProblem.ViewModel;
using HetmansProblem.ViewModel.Enums;
using System.ComponentModel;

namespace HetmansProblem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly BoardSettings _boardSettings;
        private ChessBoard _chessBoard;
        private int _boardSize;
        private Results _results;

        private readonly object _solutionLocker = new object();
        private FieldType[,] _solution;

        private BackgroundWorker _worker;

        private FieldType[,] Solution
        {
            get { lock (_solutionLocker) { return _solution; } }
        }

        public MainWindow()
        {
            InitializeComponent();
            _results = new Results();
            _boardSettings = new BoardSettings
            {
                ChessBoardDimension = 8,
                ThreadSleepTime = 150
            };

            ChessForm.DataContext = _boardSettings;
            Results.DataContext = _results;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (_worker != null && _worker.IsBusy)
            {
                _worker.RunWorkerCompleted -= Worker_ShowSolution;
                _worker.CancelAsync();
                ChangeButton();
                return;
            }

            if(!_boardSettings.IsValid())
                return;

            _results.Iterations = 0;
            _results.BadChoices = 0;

            ChangeButton();

            _worker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true,
            };

            if (_boardSettings.ThreadSleepTime > 0)
            {
                _worker.ProgressChanged += Worker_UpdateUI;
            }
            _worker.DoWork += Worker_DoWork;

            _worker.RunWorkerCompleted += Worker_ShowSolution;

            SolveEightQueens();
        }

        private void SolveEightQueens()
        {
            _boardSize = _boardSettings.ChessBoardDimension;
            _chessBoard = new ChessBoard(_boardSize);
            DrawChessBoard(_boardSize);

            _worker.RunWorkerAsync(_boardSize);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs args)
        {
            int rowCount = (int)args.Argument;
            EightQueensSolver solver = new EightQueensSolver(rowCount);
            _solution = solver.SolveProblem(_worker, ref _solution, _solutionLocker, _boardSettings.ThreadSleepTime, _results);
        }

        private void Worker_UpdateUI(object sender, ProgressChangedEventArgs args)
        {
            _chessBoard.SetChessboardSetup(_solution, _boardSize);
        }

        private void Worker_ShowSolution(object sender, RunWorkerCompletedEventArgs e)
        {
            bool hasSolution = false;
            for (int i = 0; i < _boardSize; i++)
            {
                if (Solution[0, i] == FieldType.Hetman)
                {
                    hasSolution = true;
                    break;
                }
            }
            if (hasSolution)
                _chessBoard.SetChessboardSetup(_solution, _boardSize);
            else
                MessageBox.Show("Brak rozwiązań");

            ChangeButton();
        }

        private void DrawChessBoard(int size)
        {
            InitializeChessBoardView();
            AddRectanglesWithBindingToUniformGrid(size);
        }

        private void AddRectanglesWithBindingToUniformGrid(int size)
        {
            using (Dispatcher.DisableProcessing())
            {
                for (int row = 0; row < size; row++)
                {
                    for (int column = 0; column < size; column++)
                    {
                        Binding rectangleBinding = new Binding("Color")
                        {
                            Mode = BindingMode.OneWay,
                            Source = _chessBoard[row, column],
                            Converter = new FieldColorToRectangleColorConverter()
                        };

                        Rectangle rectangle = new Rectangle();
                        rectangle.SetBinding(Shape.FillProperty, rectangleBinding);

                        Binding imageBinding = new Binding("Type")
                        {
                            Mode = BindingMode.OneWay,
                            Source = _chessBoard[row, column],
                            Converter = new FieldTypeToImageConverter(),
                            IsAsync = true
                        };

                        Image image = new Image();

                        RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.LowQuality);
                        image.SetBinding(Image.SourceProperty, imageBinding);

                        Grid grid = new Grid();
                        grid.Children.Add(rectangle);
                        grid.Children.Add(image);

                        ChessBoard.Children.Add(grid);
                    }
                }
            }
        }

        private void InitializeChessBoardView()
        {
            ChessBoard.Children.Clear();
            ChessBoard.Columns = _boardSettings.ChessBoardDimension;
            ChessBoard.Rows = _boardSettings.ChessBoardDimension;
        }

        private void ChangeButton()
        {
            string presentText = BtnSolve.Content as string;

            if (presentText == null)
            return;

            switch (presentText)
            {
                case "Oblicz":
                    ChangeButtonText("Zatrzymaj");
                    return;
                case "Zatrzymaj":
                    ChangeButtonText("Oblicz");
                    return;
            }
        }

        private void ChangeButtonText(string newText)
        {
            BtnSolve.Content = newText;
        }

        private void NFieldTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            string text = e.Text;
            e.Handled = !IsNumericText(text);
        }

        private static bool IsNumericText(string text)
        {
            Regex regex = new Regex(@"[0-9]", RegexOptions.CultureInvariant);
            return regex.IsMatch(text);
        }

        private void NFieldTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Keyboard.ClearFocus();
                _boardSettings.ChessBoardDimension = int.Parse(NFieldTextBox.Text);
                SolveEightQueens();
            }

        }
    }
}
