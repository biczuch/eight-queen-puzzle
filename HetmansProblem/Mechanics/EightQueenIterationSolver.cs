﻿using System.Collections.Generic;
using System.Linq;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.Mechanics
{
    class EightQueenIterationSolver
    {
        private readonly FieldType[,] _board;
        private readonly int _currentRow;
        private readonly int _size;

        public EightQueenIterationSolver(FieldType[,] board, int size)
        {
            _board = board;
            _size = size;

            _currentRow = GetCurrentRowFromBoard(size);
        }

        private int GetCurrentRowFromBoard(int size)
        {
            int currentRow = size - 1;
            for (int i = currentRow; i >= 0; i--)
            {
                bool hasQueen = false;
                for (int j = 0; j < _size; j++)
                {
                    if (_board[i, j] == FieldType.Hetman)
                    {
                        hasQueen = true;
                        break;
                    }
                }

                if (hasQueen == false)
                {
                    currentRow = i;
                    break;
                }
            }
            return currentRow;
        }

        public IEnumerable<FieldType[,]> Solve()
        {
            FieldType[] row = new FieldType[_size];

            for (int i = 0; i < _size; i++)
                row[i] = _board[_currentRow, i];

            int[] properFieldsIndexes = row
                .Select((t, i) => new { type = t, index = i })
                .Where(x => x.type == FieldType.Blank)
                .Select(n => n.index)
                .ToArray();

            if (properFieldsIndexes.Length == 0)
                return null;

            List<FieldType[,]> solutions = new List<FieldType[,]>();

            foreach (int properField in properFieldsIndexes)
            {
                FieldType[,] board = (FieldType[,])_board.Clone();
                board[_currentRow, properField] = FieldType.Hetman;

                board = CheckUnvailableFields(board, properField);

                solutions.Add(board);
            }

            return solutions;
        }

        private FieldType[,] CheckUnvailableFields(FieldType[,] board, int properField)
        {
            //checked fields horizontal to hetman
            for (int i = 0; i < _size; i++)
            {
                if (board[_currentRow, i] == FieldType.Hetman)
                    continue;

                board[_currentRow, i] = FieldType.Checked;
            }

            //checked fields vertical to hetman
            for (int i = _currentRow; i >= 0; i--)
            {
                if (board[i, properField] == FieldType.Hetman)
                    continue;

                board[i, properField] = FieldType.Checked;
            }

            //checked fields cross to hetman
            //up-right
            for (int i = _currentRow, j = properField; i >= 0 && j < _size; i--, j++)
            {
                if (board[i, j] == FieldType.Hetman)
                    continue;

                board[i, j] = FieldType.Checked;
            }
            //up-left
            for (int i = _currentRow, j = properField; i >= 0 && j >= 0; i--, j--)
            {
                if (board[i, j] == FieldType.Hetman)
                    continue;

                board[i, j] = FieldType.Checked;
            }

            return board;
        }
    }
}
