﻿using System.ComponentModel;
using System.Threading;
using HetmansProblem.ViewModel;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.Mechanics
{
    class EightQueensSolver
    {
        public Node RootNode { get; set; }
        public Node CurrentNode { get; set; }
        public Node PreviousNode { get; set; }

        public EightQueensSolver(int size)
        {   
            var board = CreateEmptyBoard(size);

            RootNode = new Node(board, 0, size, null, 0);
        }

        private static FieldType[,] CreateEmptyBoard(int size)
        {
            FieldType[,] board = new FieldType[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    board[i, j] = FieldType.Blank;
                }
            }
            return board;
        }

        public FieldType[,] SolveProblem(Results results)
        {
            CurrentNode = RootNode;

            while (CurrentNode != null)
            {
                PreviousNode = CurrentNode;
                CurrentNode = CurrentNode.Solve(results);
            }

            return PreviousNode == RootNode ? null : PreviousNode.ChessBoard;
        }

        public FieldType[,] SolveProblem(BackgroundWorker worker, ref FieldType[,] solutionToUpdate, object solutionLocker, int threadSleepTimeInMs, Results results)
        {
            CurrentNode = RootNode;

            while (CurrentNode != null)
            {
                if (worker.CancellationPending)
                {
                    //worker.CancelAsync();
                    break;
                }
                    

                PreviousNode = CurrentNode;
                CurrentNode = CurrentNode.Solve(results);

                lock (solutionLocker)
                {
                    solutionToUpdate = CurrentNode.ChessBoard;
                    worker.ReportProgress(0);
                }
                Thread.Sleep(threadSleepTimeInMs);
            }

            return PreviousNode == RootNode ? null : PreviousNode.ChessBoard;
        }
    }
}
