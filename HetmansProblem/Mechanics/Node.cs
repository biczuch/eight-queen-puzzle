﻿using System;
using System.Collections.Generic;
using System.Linq;
using HetmansProblem.ViewModel;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.Mechanics
{
    class Node
    {
        public FieldType[,] ChessBoard { get; private set; }
        public int Size { get; set; }
        private readonly double _rank;
        public List<Node> Solutions { get; set; }
        public Node ParentNode { get; set; }
        public int NumberOfQeens { get; set; }
        private int _primarySolutionCount;

        public Node(FieldType[,] chessBoard, double rank, int size, Node parentNode, int numberOfQueens)
        {
            ChessBoard = chessBoard;
            Size = size;
            _rank = rank;
            ParentNode = parentNode;
            NumberOfQeens = numberOfQueens;
        }

        public double Rank
        {
            get { return _rank; }
        }

        public Node Solve(Results results)
        {
            if (NumberOfQeens == Size)
                return null;

            if (Solutions != null)
                return SolveNext(results);

            EightQueenIterationSolver solver = new EightQueenIterationSolver(ChessBoard, Size);

            IEnumerable<FieldType[,]> solutionBoards = solver.Solve();
            if (solutionBoards == null)
                return ParentNode;

            Solutions = new List<Node>();
            foreach (FieldType[,] board in solutionBoards)
            {
                double rank = RankSolution(board);
                Node node = new Node(board, rank, Size, this, NumberOfQeens + 1);
                Solutions.Add(node);
            }

            Solutions = Solutions.OrderByDescending(n => n.Rank).ToList();
            _primarySolutionCount = Solutions.Count;

            return SolveNext(results);
        }

        private double RankSolution(FieldType[,] board)
        {
            int hetmanRow;
            int hetmanColumn;

            GetLastHetmanPosition(board, out hetmanRow, out hetmanColumn);

            int numberOfFreeFieldsNextRow = 0;

            if (hetmanRow != 0)
            {
                for (int i = 0; i < Size; i++)
                {
                    if (board[i, hetmanRow - 1] == FieldType.Blank)
                        numberOfFreeFieldsNextRow++;
                }
            }

            double halfBoardPosition = (double)Math.Floor((decimal)Size/2);

            double position = halfBoardPosition - Math.Abs(halfBoardPosition - hetmanColumn);
            
            double rank = position - numberOfFreeFieldsNextRow;

            return rank;
        }

        private void GetLastHetmanPosition(FieldType[,] board, out int lastHetmanRow, out int lastHetmanColumn)
        {
            lastHetmanRow = 0;
            lastHetmanColumn = 0;
            bool stop = false;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (board[i, j] == FieldType.Hetman)
                    {
                        lastHetmanRow = i;
                        lastHetmanColumn = j;
                        stop = true;
                        break;
                    }
                }
                if (stop)
                    break;
            }
        }

        private Node SolveNext(Results results)
        {
            results.Iterations++;
            if (Solutions.Any())
            {
                if (Solutions.Count != _primarySolutionCount)
                    results.BadChoices++;

                Node nextNode = Solutions.ElementAt(0);
                Solutions.RemoveAt(0);
                return nextNode;
            }

            results.BadChoices++;
            return ParentNode;
        }
    }
}
