﻿using System.ComponentModel;

namespace HetmansProblem.ViewModel
{
    class BoardSettings : IDataErrorInfo
    {
        public int ChessBoardDimension { get; set; }
        public int ThreadSleepTime { get; set; }

        public static string[] PropertyNames =
        {
            "ChessBoardDimension",
            "ThreadSleepTime"
        };

        public string this[string propertyName]
        {
            get { return ValidateProperty(propertyName); }
        }

        protected string ValidateProperty(string propertyName)
        {
           switch (propertyName)
            {
                case "ChessBoardDimension":
                    if (ChessBoardDimension.GetType() != typeof (int))
                        return "Nieprawidłowa wartość parametru.";
                    if (ChessBoardDimension < 1 || ChessBoardDimension > 50)
                        return "Wielość pola gry musi mieścić się w przedziale <1;50>";
                    break;
                case "ThreadSleepTime":
                    if (ThreadSleepTime < 0 || ThreadSleepTime > 5000)
                        return "Czas oczekiwania na rysowanie musi mieścić się w przedziale <0;5000>";
                    break;
            }

            return null;
        }

        public string Error { get; private set; }

        public bool IsValid()
        {
            foreach (string propertyName in PropertyNames)
            {
                if (ValidateProperty(propertyName) != null)
                    return false;
            }

            return true;
        }
    }
}
