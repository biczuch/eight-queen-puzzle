﻿using System;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.ViewModel
{
    public class ChessBoard
    {
        public Field[,] Board { get; set; }
        public int Size { get; private set; }

        public ChessBoard(int size)
        {
            Size = size;
            Board = AddFieldsToChessBoard(size);
        }

        public Field this[int row, int column]
        {
            get { return Board[row, column]; }
        }

        private static Field[,] AddFieldsToChessBoard(int size)
        {
            int numberOfFields = (int)Math.Pow(size, 2);
            Field[,] board = new Field[size, size];

            int row = -1;
            int column = 0;

            if (numberOfFields % 2 == 0)
            {
                bool inverted = true;
                for (int i = 0; i < numberOfFields; i++)
                {
                    column = i % size;
                    if (i % size == 0)
                    {
                        inverted = !inverted;
                        row = row + 1;
                    }

                    Field field;

                    if (inverted)
                    {
                        if (i % 2 == 0)
                            field = new Field { Color = FieldColor.Black };
                        else
                            field = new Field { Color = FieldColor.White };
                    }
                    else
                    {
                        if (i % 2 == 0)
                            field = new Field { Color = FieldColor.White };
                        else
                            field = new Field { Color = FieldColor.Black };
                    }

                    board[row, column] = field;
                }
            }
            else
            {
                for (int i = 0; i < numberOfFields; i++)
                {
                    column = i % size;
                    if (i % size == 0)
                        row = row + 1;

                    Field field;

                    if (i % 2 == 0)
                        field = new Field { Color = FieldColor.Black };
                    else
                        field = new Field { Color = FieldColor.White };

                    board[row, column] = field;
                }
            }
            return board;
        }

        public void SetChessboardSetup(FieldType[,] newSetup, int size)
        {
            if(size != Size)
                throw new ArgumentException("Sizes does not match!");

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if(Board[i, j].Type != newSetup[i, j])
                    {
                        Board[i, j].Type = newSetup[i, j];
                    } 
                }
            }
        }

        public FieldType[,] GetChessboardSetup()
        {
            FieldType[,] convertedChessBoard = new FieldType[Size, Size];

            for (int row = 0; row < Size; row++)
            {
                for (int column = 0; column < Size; column++)
                {
                    convertedChessBoard[row, column] = Board[row, column].Type;
                }
            }

            return convertedChessBoard;
        }
    }
}
