﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HetmansProblem.Annotations;
using HetmansProblem.ViewModel.Enums;

namespace HetmansProblem.ViewModel
{
    public class Field : INotifyPropertyChanged
    {
        private FieldType _type;
        private FieldColor _color;

        public FieldType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }

        public FieldColor Color
        {
            get { return _color; }
            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
