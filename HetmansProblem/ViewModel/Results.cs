﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HetmansProblem.Annotations;

namespace HetmansProblem.ViewModel
{
    class Results : INotifyPropertyChanged
    {
        private int _iterations;
        private int _badChoices;

        public int Iterations
        {
            get { return _iterations; }
            set
            {
                _iterations = value;
                OnPropertyChanged("Iterations");
            }
        }

        public int BadChoices
        {
            get { return _badChoices; }
            set
            {
                _badChoices = value;
                OnPropertyChanged("BadChoices");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
